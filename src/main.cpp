#include <string>

#include "bot/PictureGameBot.h"

// TODO: Add proper config file
#include "config.h"

using namespace PictureGame;
using namespace Bot;
using namespace Utils;

int main() {
    Config config {
        Username,
        Password,
        ClientToken,
        ClientSecret,

        DefaultLogLevel,

        "localhost",
        ApiPort,
        ApiUsername,
        ApiPassword,

        SubredditName,
    };
    PictureGameBot bot(config);

    try {
        bot.Run();
    } catch (const std::exception& e) {
        bot.GetLogger().Fatal(e.what());
        return 1;
    }
}
