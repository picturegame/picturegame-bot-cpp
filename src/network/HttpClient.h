#pragma once

#include <boost/beast/core/error.hpp>
#include <boost/beast/core/flat_buffer.hpp>

#include "HttpRequest.h"

#include "utils/Logger.h"

namespace beast = boost::beast;
namespace http = beast::http;

namespace PictureGame::Network {

template <typename Session>
class HttpClient {
    public:
    using Response_t = http::response<http::string_body>;
    using TaskHandle_t = typename Session::TaskHandle_t;

    HttpClient(const char* host, Utils::Logger& logger):
        _host(host),
        _port(nullptr),
        _session(logger),
        _logger(logger)
    {
        Connect();
    }

    HttpClient(const char* host, const char* port, Utils::Logger& logger):
        _host(host),
        _port(port),
        _session(logger),
        _logger(logger)
    {
        Connect();
    }

    HttpClient(const HttpClient& other) = delete;
    HttpClient& operator=(const HttpClient& other) = delete;
    HttpClient(HttpClient&& other) = delete;
    HttpClient& operator=(HttpClient&& other) = delete;

    ~HttpClient() {
        TaskHandle_t handle;
        _session.ShutDown(handle);
        auto [message, ec] = handle.GetResult();
        LogTaskResult(message, ec);
    }

    Response_t SendRequest(HttpRequest&& request) {
        request.SetHost(_host);

        Response_t res;
        beast::flat_buffer buf;

        TaskHandle_t handle;
        _session.Write(request.GetRequest(), buf, res, handle);
        auto [message, ec] = handle.GetResult();
        LogTaskResult(message, ec);

        return res;
    }

    private:
    const char* _host;
    const char* _port;

    Session _session;
    Utils::Logger& _logger;

    void Connect() {
        TaskHandle_t handle;
        _session.Connect(_host, _port, handle);
        auto [message, ec] = handle.GetResult();
        LogTaskResult(message, ec);
    }

    void LogTaskResult(const char* message, beast::error_code ec) {
        if (ec) {
            _logger.Error(message, {{ "e", ec.message() }});
        } else {
            _logger.Verbose(message, {{ "status", "done" }});
        }
    }
};

}
