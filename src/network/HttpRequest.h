#pragma once

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/http/verb.hpp>
#include <boost/beast/version.hpp>

#include <rapidjson/document.h>

#include <string>
#include <unordered_map>

namespace http = boost::beast::http;
using tcp = boost::asio::ip::tcp;

namespace PictureGame::Network {

constexpr int HttpVersion = 11;
constexpr auto UserAgent = BOOST_BEAST_VERSION_STRING ";picturegame-bot/2.0 (By /u/Provium)";

class HttpRequest {
    public:
    using FormData_t = std::unordered_map<std::string, std::string>;

    HttpRequest(const std::string& path, http::verb verb);

    HttpRequest(const HttpRequest& other) = delete;
    HttpRequest& operator=(const HttpRequest& other) = delete;
    HttpRequest(HttpRequest&& other) = default;
    HttpRequest& operator=(HttpRequest&& other) = default;

    void SetFormData(FormData_t&& data);
    void SetJsonData(const rapidjson::Document& data);
    void SetHost(const char* host);

    void BasicAuth(const std::string& username, const std::string& password);
    void BearerAuth(const std::string& token);

    const http::request<http::string_body>& GetRequest() const noexcept;

    private:
    http::request<http::string_body> _req;
};

}
