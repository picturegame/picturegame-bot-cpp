#pragma once

#include <thread>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/context.hpp>

#include <boost/beast/core/error.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>

#include "threading/TaskHandle.h"

#include "utils/Logger.h"

namespace beast = boost::beast;
namespace http = beast::http;
namespace asio = boost::asio;
namespace ssl = asio::ssl;
using tcp = boost::asio::ip::tcp;

namespace PictureGame::Network {

class SslSession {
    public:
    using TaskHandle_t = Threading::TaskHandle<const char*, beast::error_code>;

    explicit SslSession(Utils::Logger& logger);

    SslSession(const SslSession& other) = delete;
    SslSession& operator=(const SslSession& other) = delete;
    SslSession(SslSession&& other) = delete;
    SslSession& operator=(SslSession&& other) = delete;

    ~SslSession();

    void Connect(const char* host, const char* port, TaskHandle_t& handle);

    void Write(
        const http::request<http::string_body>& req,
        beast::flat_buffer& buffer,
        http::response<http::string_body>& res,
        TaskHandle_t& handle);

    void ShutDown(TaskHandle_t& handle);

    private:
    asio::io_context _ioCtx;
    asio::executor_work_guard<asio::io_context::executor_type> _workGuard;
    std::thread _ioThread;

    ssl::context _sslCtx{ssl::context::tlsv12_client};

    tcp::resolver _resolver;
    beast::ssl_stream<beast::tcp_stream> _stream;
    Utils::Logger& _logger;

    // Callbacks for connection
    void OnResolve(TaskHandle_t& handle, beast::error_code ec, const tcp::resolver::results_type& results);
    void OnConnect(TaskHandle_t& handle, beast::error_code ec);

    // Callbacks for requests
    void OnWrite(
        beast::flat_buffer& buffer,
        http::response<http::string_body>& res,
        TaskHandle_t& handle,
        beast::error_code ec);
};

}
