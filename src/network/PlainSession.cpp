#include "PlainSession.h"

#include <boost/asio/strand.hpp>

#include <boost/beast/http/read.hpp>
#include <boost/beast/http/write.hpp>

namespace PictureGame::Network {

using namespace Utils;

namespace {
    constexpr auto HttpPort = "80";
}

PlainSession::PlainSession(Logger& logger):
    _workGuard(asio::make_work_guard(_ioCtx)),
    _ioThread([this] { _ioCtx.run(); }),
    _stream(asio::make_strand(_ioCtx)),
    _resolver(asio::make_strand(_ioCtx)),
    _logger(logger)
{}

PlainSession::~PlainSession() {
    _ioCtx.stop();
    _ioThread.join();
}

void PlainSession::Connect(const char* host, const char* port, TaskHandle_t& handle) {
    _resolver.async_resolve(host, port == nullptr ? HttpPort : port,
        [this, &handle] (beast::error_code ec, const tcp::resolver::results_type& results) {
            OnResolve(handle, ec, results);
        });
}

void PlainSession::OnResolve(TaskHandle_t& handle, beast::error_code ec, const tcp::resolver::results_type& results) {
    if (ec) {
        return handle.Complete("Resolve", ec);
    }

    _stream.expires_after(std::chrono::seconds(30));

    _stream.async_connect(results,
        [&handle](beast::error_code ec, const tcp::resolver::results_type::endpoint_type&) {
            if (ec) {
                return handle.Complete("Connect", ec);
            }

            handle.Complete("Connect", {});
        });
}

void PlainSession::Write(
    const http::request<http::string_body>& req,
    beast::flat_buffer& buffer,
    http::response<http::string_body>& res,
    TaskHandle_t& handle)
{
    _stream.expires_after(std::chrono::seconds(30));

    http::async_write(_stream, req,
        [this, &buffer, &res, &handle](beast::error_code ec, size_t) {
            OnWrite(buffer, res, handle, ec);
        });
}

void PlainSession::OnWrite(
    beast::flat_buffer& buffer,
    http::response<http::string_body>& res,
    TaskHandle_t& handle,
    beast::error_code ec)
{
    if (ec) {
        return handle.Complete("Write", ec);
    }

    http::async_read(_stream, buffer, res,
        [&handle](beast::error_code ec, size_t) {
            if (ec) {
                return handle.Complete("Read", ec);
            }

            handle.Complete("Request", {});
        });
}

void PlainSession::ShutDown(TaskHandle_t& handle) {
    beast::error_code ec;
    _stream.socket().shutdown(tcp::socket::shutdown_both, ec);

    if (ec && ec != beast::errc::not_connected) {
        return handle.Complete("Shutdown", ec);
    }

    handle.Complete("Shutdown", {});
}

}
