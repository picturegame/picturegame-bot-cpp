#include <sstream>
#include <string>

#include <boost/beast/http/field.hpp>
#include <boost/beast/http/read.hpp>

#include "HttpRequest.h"

#include "utils/JsonUtils.h"
#include "utils/String.h"

namespace PictureGame::Network {

using namespace Utils;

HttpRequest::HttpRequest(const std::string& path, http::verb verb) :
    _req(verb, path, HttpVersion)
{
    _req.set(http::field::user_agent, UserAgent);
}

void HttpRequest::SetFormData(FormData_t&& data) {
    _req.body() = ToFormEncoded(data);
    _req.set(http::field::content_type, "application/x-www-form-urlencoded");
    _req.prepare_payload();
}

void HttpRequest::SetJsonData(const rapidjson::Document& data) {
    _req.body() = JsonToString(data);
    _req.set(http::field::content_type, "application/json");
    _req.prepare_payload();
}

void HttpRequest::SetHost(const char* host) {
    _req.set(http::field::host, host);
}

void HttpRequest::BasicAuth(const std::string& username, const std::string& password) {
    auto basicToken = Base64Encode(username + ':' + password);
    _req.set(http::field::authorization, "Basic " + basicToken);
}

void HttpRequest::BearerAuth(const std::string& token) {
    _req.set(http::field::authorization, "Bearer " + token);
}

const http::request<http::string_body>& HttpRequest::GetRequest() const noexcept {
    return _req;
}

}
