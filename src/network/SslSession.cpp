#include "SslSession.h"

#include <chrono>

#include <boost/asio/error.hpp>
#include <boost/asio/executor_work_guard.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <boost/beast/core/bind_handler.hpp>
#include <boost/beast/http/read.hpp>
#include <boost/beast/http/write.hpp>

namespace PictureGame::Network {

using namespace Utils;

namespace {
    constexpr auto SslPort = "443";
}

SslSession::SslSession(Logger& logger):
    _workGuard(asio::make_work_guard(_ioCtx)),
    _ioThread([this] { _ioCtx.run(); }),
    _resolver(asio::make_strand(_ioCtx)),
    _stream(asio::make_strand(_ioCtx), _sslCtx),
    _logger(logger)
{}

SslSession::~SslSession() {
    _ioCtx.stop();
    _ioThread.join();
}

void SslSession::Connect(const char* host, const char* port, TaskHandle_t& handle) {
    if (!SSL_set_tlsext_host_name(_stream.native_handle(), host)) {
        return handle.Complete("set_tlsext_host_name",
            {static_cast<int>(::ERR_get_error()), asio::error::get_ssl_category()});
    }

    _resolver.async_resolve(host, port == nullptr ? SslPort : port,
        [this, &handle](beast::error_code ec, const tcp::resolver::results_type& results) {
            OnResolve(handle, ec, results);
        });
}

void SslSession::OnResolve(TaskHandle_t& handle, beast::error_code ec, const tcp::resolver::results_type& results) {
    if (ec) {
        return handle.Complete("Resolve", ec);
    }

    beast::get_lowest_layer(_stream).expires_after(std::chrono::seconds(30));

    beast::get_lowest_layer(_stream).async_connect(results,
        [this, &handle](beast::error_code ec, const tcp::resolver::results_type::endpoint_type&) {
            OnConnect(handle, ec);
        });
}

void SslSession::OnConnect(TaskHandle_t& handle, beast::error_code ec) {
    if (ec) {
        return handle.Complete("Connect", ec);
    }

    _stream.async_handshake(ssl::stream_base::client,
        [&handle](beast::error_code ec) {
            if (ec) {
                return handle.Complete("Handshake", ec);
            }
            // Successfully connected (caller will receive empty ec)
            handle.Complete("Connect", {});
        });
}

void SslSession::Write(
    const http::request<http::string_body>& req,
    beast::flat_buffer& buffer,
    http::response<http::string_body>& res,
    TaskHandle_t& handle)
{
    // TODO: Make timeout configurable?
    beast::get_lowest_layer(_stream).expires_after(std::chrono::seconds(30));

    http::async_write(_stream, req,
        [this, &buffer, &res, &handle](beast::error_code ec, size_t) {
            OnWrite(buffer, res, handle, ec);
        });
}

void SslSession::OnWrite(
    beast::flat_buffer& buffer,
    http::response<http::string_body>& res,
    TaskHandle_t& handle,
    beast::error_code ec)
{
    if (ec) {
        return handle.Complete("Write", ec);
    }

    http::async_read(_stream, buffer, res,
        [&handle](beast::error_code ec, size_t) {
            if (ec) {
                return handle.Complete("Read", ec);
            }

            // Request successful (caller will receive empty ec)
            handle.Complete("Request", {});
        });
}

void SslSession::ShutDown(TaskHandle_t& handle) {
    beast::get_lowest_layer(_stream).expires_after(std::chrono::seconds(10));
    _stream.async_shutdown(
        [&handle](beast::error_code ec) {
            if (ec == asio::error::eof) {
                ec = {};
            }
            handle.Complete("Shutdown", ec);
        });
}

}
