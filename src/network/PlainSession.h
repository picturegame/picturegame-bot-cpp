#pragma once

#include <thread>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>

#include <boost/beast/core/error.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http/message.hpp>
#include <boost/beast/http/string_body.hpp>

#include "threading/TaskHandle.h"

#include "utils/Logger.h"

namespace beast = boost::beast;
namespace http = beast::http;
namespace asio = boost::asio;
using tcp = boost::asio::ip::tcp;

namespace PictureGame::Network {

class PlainSession {
    public:
    using TaskHandle_t = Threading::TaskHandle<const char*, beast::error_code>;

    explicit PlainSession(Utils::Logger& logger);

    PlainSession(const PlainSession& other) = delete;
    PlainSession& operator=(const PlainSession& other) = delete;
    PlainSession(PlainSession&& other) = delete;
    PlainSession& operator=(PlainSession&& other) = delete;

    ~PlainSession();

    void Connect(const char* host, const char* port, TaskHandle_t& handle);

    void Write(
        const http::request<http::string_body>& req,
        beast::flat_buffer& buffer,
        http::response<http::string_body>& res,
        TaskHandle_t& handle);

    void ShutDown(TaskHandle_t& handle);

    private:
    asio::io_context _ioCtx;
    asio::executor_work_guard<asio::io_context::executor_type> _workGuard;
    std::thread _ioThread;

    beast::tcp_stream _stream;
    tcp::resolver _resolver;
    Utils::Logger& _logger;

    // Callbacks for connection
    void OnResolve(TaskHandle_t& handle, beast::error_code ec, const tcp::resolver::results_type& results);

    // Callbacks for requests
    void OnWrite(
        beast::flat_buffer& buffer,
        http::response<http::string_body>& res,
        TaskHandle_t& handle,
        beast::error_code ec);
};

}
