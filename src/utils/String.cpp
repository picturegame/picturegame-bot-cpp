#include "String.h"

#include <sstream>
#include <string>

namespace PictureGame::Utils {

std::string Urlencode(const std::string& input) {
    constexpr auto lookup = "0123456789abcdef";
    std::ostringstream stream;

    for (const char& c : input) {
        if (('0' <= c && c <= '9') || ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z')) {
            stream << c;
        } else {
            stream << '%';
            stream << lookup[(c & 0xF0) >> 4];
            stream << lookup[(c & 0x0F)];
        }
    }
    return stream.str();
}

std::string ToFormEncoded(std::unordered_map<std::string, std::string> data) {
    std::ostringstream stream;
    for (const auto& [key, value] : data) {
        stream << key;
        stream << '=';
        stream << Urlencode(value);
        stream << '&';
    }
    return stream.str();
}

namespace {
void Convert3To4(const char* char3, std::ostringstream& outputStream) {
    constexpr auto base64Chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";

    int char4[4] {
        (char3[0] & 0xfc) >> 2,
        ((char3[0] & 0x03) << 4) + ((char3[1] & 0xf0) >> 4),
        ((char3[1] & 0x0f) << 2) + ((char3[2] & 0xc0) >> 6),
        char3[2] & 0x3f,
    };

    for (auto c : char4) {
        outputStream << base64Chars[c];
    }
}
}

std::string Base64Encode(const std::string& input) {
    std::ostringstream stream;
    int i = 0;
    char char3[3];

    auto iter = input.begin();
    while (iter != input.end()) {
        char3[i++] = *(iter++);
        if (i == 3) {
            Convert3To4(char3, stream);
            i = 0;
        }
    }

    if (i) {
        for (int j = i; j < 3; j++) {
            char3[j] = '\0';
        }

        Convert3To4(char3, stream);
        while ((i++ < 3)) {
            stream << '=';
        }
    }

    return stream.str();
}

}
