#pragma once

#include <deque>
#include <unordered_set>

namespace PictureGame::Utils {

template <typename T>
class FifoSet {
    public:
    explicit FifoSet(size_t capacity): _capacity(capacity) {}

    FifoSet(const FifoSet<T>& other) = delete;
    FifoSet& operator=(const FifoSet<T>& other) = delete;
    FifoSet(FifoSet<T>&& other) noexcept = default;
    FifoSet& operator=(FifoSet<T>&& other) noexcept = default;

    bool Contains(const T& item) {
        return _set.find(item) != _set.end();
    }

    void Add(T item) {
        if (Contains(item)) {
            return;
        }

        if (_queue.size() >= _capacity) {
            const auto oldest = _queue.front();
            _queue.pop_front();
            _set.erase(oldest);
        }

        _queue.push_back(item);
        _set.insert(item);
    }

    private:
    size_t _capacity;

    std::deque<T> _queue;
    std::unordered_set<T> _set;
};

}
