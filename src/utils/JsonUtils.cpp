#include "JsonUtils.h"

#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

namespace PictureGame::Utils {

std::string JsonToString(const JsonDoc_t& data) {
    rapidjson::StringBuffer buf;
    rapidjson::Writer<rapidjson::StringBuffer> writer(buf);
    data.Accept(writer);
    return buf.GetString();
}

}
