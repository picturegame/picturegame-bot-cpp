#include <string>

#include <rapidjson/document.h>

using JsonDoc_t = rapidjson::Document;
using JsonValue_t = JsonDoc_t::ValueType;
using JsonAllocator_t = JsonDoc_t::AllocatorType;
using JsonArray_t = JsonValue_t::Array;
using JsonObject_t = JsonValue_t::Object;

namespace PictureGame::Utils {

std::string JsonToString(const JsonDoc_t& doc);

}
