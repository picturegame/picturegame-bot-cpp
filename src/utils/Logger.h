#pragma once

#include <mutex>
#include <string>
#include <string_view>
#include <unordered_map>

namespace PictureGame::Utils {

enum class LogLevel {
    Verbose,
    Debug,
    Info,
    Warn,
    Error,
    Fatal,
};

class Logger {
    public:
    using LogData_t = std::unordered_map<std::string, std::string>;

    explicit Logger(LogLevel level);

    void Fatal(std::string_view message, LogData_t&& data = {}) {
        Log(LogLevel::Fatal, message, std::forward<LogData_t>(data));
    }
    void Error(std::string_view message, LogData_t&& data = {}) {
        Log(LogLevel::Error, message, std::forward<LogData_t>(data));
    }
    void Warn(std::string_view message, LogData_t&& data = {}) {
        Log(LogLevel::Warn, message, std::forward<LogData_t>(data));
    }
    void Info(std::string_view message, LogData_t&& data = {}) {
        Log(LogLevel::Info, message, std::forward<LogData_t>(data));
    }
    void Debug(std::string_view message, LogData_t&& data = {}) {
        Log(LogLevel::Debug, message, std::forward<LogData_t>(data));
    }
    void Verbose(std::string_view message, LogData_t&& data = {}) {
        Log(LogLevel::Verbose, message, std::forward<LogData_t>(data));
    }

    private:
    std::mutex _mtx;
    LogLevel _level;

    void Log(LogLevel level, std::string_view message, LogData_t&& data = {});
};

}
