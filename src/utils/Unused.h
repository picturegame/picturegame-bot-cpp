#pragma once

namespace PictureGame::Utils {

template <typename ...Args>
inline constexpr void Unused(Args&&...) { }

}
