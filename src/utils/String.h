#pragma once

#include <string>
#include <unordered_map>

namespace PictureGame::Utils {

std::string Urlencode(const std::string& input);

std::string ToFormEncoded(std::unordered_map<std::string, std::string>);

std::string Base64Encode(const std::string& input);

}
