#include "Logger.h"

#include <iostream>
#include <string>

namespace PictureGame::Utils {

namespace {
std::string LogLevelToString(LogLevel level) {
    switch (level) {
        case LogLevel::Verbose: return "Verbose";
        case LogLevel::Debug: return "Debug";
        case LogLevel::Info: return "Info";
        case LogLevel::Warn: return "Warn";
        case LogLevel::Error: return "Error";
        case LogLevel::Fatal: return "Fatal";
        default: throw std::runtime_error("Unknown log level");
    }
}
}

Logger::Logger(LogLevel level): _level(level) {}

void Logger::Log(LogLevel level, std::string_view message, LogData_t&& data) {
    if (level < _level) {
        return;
    }

    std::scoped_lock lock(_mtx);

    std::cout << LogLevelToString(level) << ": " << message;
    for (const auto& [key, value]: data) {
        std::cout << ' ' << key << " = " << value << ',';
    }
    std::cout << '\n';
}

}
