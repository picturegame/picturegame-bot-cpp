#include "RedditClient.h"

#include <limits>
#include <string>

#include "utils/String.h"

namespace PictureGame::Reddit {

using namespace Network;
using namespace Model;
using namespace Utils;

constexpr auto OauthUrl = "oauth.reddit.com";

constexpr auto RateLimitRemainingHeader = "x-ratelimit-remaining";
constexpr auto RateLimitResetHeader = "x-ratelimit-reset";

RedditClient::RedditClient(
    std::string username,
    std::string password,
    std::string clientToken,
    std::string clientSecret,
    Logger& logger):

    _client(OauthUrl, logger),
    _auth(std::move(username), std::move(password), std::move(clientToken), std::move(clientSecret), logger),
    _logger(logger),
    _rateLimitRemaining(std::numeric_limits<double>::max())
{}

rapidjson::Document RedditClient::MakeRequest(
    const std::string& path,
    http::verb verb,
    Network::HttpRequest::FormData_t&& query,
    Network::HttpRequest::FormData_t&& data)
{
    // TODO: handle rate limiting

    query["raw_json"] = "1";
    Network::HttpRequest req(path + "?" + ToFormEncoded(query), verb);
    if (!data.empty()) {
        req.SetFormData(std::move(data));
    }
    req.BearerAuth(_auth.GetToken());

    auto res = _client.SendRequest(std::move(req));

    int rateLimitResetSeconds = std::stoi(res[RateLimitResetHeader].to_string());
    _rateLimitResetTime = std::chrono::steady_clock::now() + std::chrono::seconds(rateLimitResetSeconds);
    _rateLimitRemaining = std::stof(res[RateLimitRemainingHeader].to_string());

    rapidjson::Document doc;
    doc.Parse(res.body().c_str());
    return doc;
}

Submission& RedditClient::GetOrCreateSubmission(const std::string& id) {
    const auto existing = _submissionLookup.find(id);
    if (existing != _submissionLookup.end()) {
        return *existing->second;
    }

    const auto res = _submissionLookup.emplace(id, std::make_unique<Submission>(*this, id));
    return *res.first->second;
}

}
