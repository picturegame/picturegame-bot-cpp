#include "Submission.h"

#include <memory>

#include "Comment.h"

#include "reddit/RedditClient.h"

namespace PictureGame::Reddit::Model {

namespace Fields {
    static constexpr auto Id = "id";
    static constexpr auto Author = "author";
    static constexpr auto BannedBy = "banned_by";
    static constexpr auto CreatedUtc = "created_utc";
    static constexpr auto IsSelf = "is_self";
    static constexpr auto LinkFlairText = "link_flair_text";
    static constexpr auto Thumbnail = "thumbnail";
    static constexpr auto Title = "title";
    static constexpr auto Url = "url";
}

Submission::Submission(RedditClient& client, std::string id): Replyable(client, std::move(id)) {}

Submission::Submission(RedditClient& client, std::string id, const JsonValue_t& json):
    Replyable(client, std::move(id)), _fetched(true)
{
    _submissionJson.CopyFrom(json, _submissionJson.GetAllocator());
}

std::string Submission::AuthorName() { return SubmissionData()[Fields::Author].GetString(); }
double Submission::CreatedUtc() { return SubmissionData()[Fields::CreatedUtc].GetDouble(); }
bool Submission::IsSelf() { return SubmissionData()[Fields::IsSelf].GetBool(); }
std::string Submission::LinkFlairText() { return SubmissionData()[Fields::LinkFlairText].GetString(); }
std::string Submission::Thumbnail() { return SubmissionData()[Fields::Thumbnail].GetString(); }
std::string Submission::Title() { return SubmissionData()[Fields::Title].GetString(); }
std::string Submission::Url() { return SubmissionData()[Fields::Url].GetString(); }

bool Submission::IsDeleted() { return SubmissionData()[Fields::Author].IsNull(); }
bool Submission::IsRemoved() { return !SubmissionData()[Fields::BannedBy].IsNull(); }

Comment* Submission::GetComment(const std::string& id) const {
    const auto existing = _commentLookup.find(id);
    if (existing == _commentLookup.end()) {
        return nullptr;
    }
    return existing->second.get();
}

Comment& Submission::AddCommentIfNotExists(const std::string& id, JsonValue_t& doc) {
    const auto existing = _commentLookup.find(id);
    if (existing != _commentLookup.end()) {
        return *existing->second;
    }

    const auto res = _commentLookup.emplace(id, std::make_unique<Comment>(_client, id, doc));
    return *res.first->second;
}

void Submission::FetchIfNecessary() {
    if (!_fetched) {
        auto fullDoc = _client.MakeRequest("comments/" + Id(), http::verb::get);
        _submissionJson.CopyFrom(fullDoc[0]["data"]["children"][0]["data"], _submissionJson.GetAllocator());
        _commentJson.CopyFrom(fullDoc[1]["data"]["children"], _commentJson.GetAllocator());
        _fetched = true;
    }
}

}
