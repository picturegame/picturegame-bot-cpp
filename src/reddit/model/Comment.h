#pragma once

#include <memory>
#include <unordered_set>

#include "Replyable.h"

#include "utils/JsonUtils.h"

namespace PictureGame::Reddit::Model {

class Submission;

class Comment : public Replyable {
    public:
    Comment(RedditClient& client, std::string id);
    Comment(RedditClient& client, std::string id, const JsonValue_t& json);

    Comment(const Comment& other) = delete;
    Comment& operator=(const Comment& other) = delete;
    Comment(Comment&& other) = delete;
    Comment& operator=(Comment&& other) = delete;

    std::string FullName() const final { return _type + Id(); }

    std::string AuthorName();
    std::string AuthorFlairCssClass();
    std::string AuthorFlairTemplateId();
    std::string AuthorFlairText();
    std::string BodyMarkdown();
    long CreatedUtc();
    bool IsSubmitter();
    std::string LinkName(); // t3_
    std::string ParentName(); // t1_ or t3_
    bool IsStickied();

    std::string SubmissionId();
    std::string ParentId();

    bool IsDeleted();
    bool IsRemoved();
    bool IsRoot();

    Submission& GetSubmission();
    Comment* GetParent();

    private:
    static constexpr auto _type = "t1_";

    bool _fetched{false};
    JsonDoc_t _json;

    std::unordered_set<std::string> _children;

    void Refresh();
    void Explore(Comment* parent, JsonValue_t& comment);
    void FetchIfNecessary();

    const JsonDoc_t& CommentData() {
        FetchIfNecessary();
        return _json;
    }
};

}
