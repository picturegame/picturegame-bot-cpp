#include "Replyable.h"

#include "reddit/RedditClient.h"

namespace PictureGame::Reddit::Model {

constexpr auto CommentPath = "api/comment";

Replyable::Replyable(RedditClient& client, std::string id): Entity(client, std::move(id)) {}

void Replyable::Reply(std::string&& message) {
    _client.MakeRequest(CommentPath, http::verb::post, {}, {
        { "text", std::move(message) },
        { "thing_id", FullName() },
    });
}

}
