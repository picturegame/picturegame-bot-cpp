#include "Comment.h"
#include "Submission.h"

#include "reddit/RedditClient.h"
#include "reddit/Utils.h"

namespace PictureGame::Reddit::Model {

namespace Fields {
    constexpr auto Author = "author";
    constexpr auto AuthorFlairCssClass = "author_flair_css_class";
    constexpr auto AuthorFlairTemplateId = "author_flair_template_id";
    constexpr auto AuthorFlairText = "author_flair_text";
    constexpr auto BannedBy = "banned_by";
    constexpr auto BodyMarkdown = "body";
    constexpr auto CreatedUtc = "created_utc";
    constexpr auto IsSubmitter = "is_submitter";
    constexpr auto LinkId = "link_id";
    constexpr auto ParentId = "parent_id";
    constexpr auto Stickied = "stickied";
}

Comment::Comment(RedditClient& client, std::string id): Replyable(client, std::move(id)) {}

Comment::Comment(RedditClient& client, std::string id, const JsonValue_t& json):
    Replyable(client, std::move(id)), _fetched(true)
{
    _json.CopyFrom(json, _json.GetAllocator());
}

std::string Comment::AuthorName() { return CommentData()[Fields::Author].GetString(); }
std::string Comment::AuthorFlairCssClass() { return CommentData()[Fields::AuthorFlairCssClass].GetString(); }
std::string Comment::AuthorFlairTemplateId() { return CommentData()[Fields::AuthorFlairTemplateId].GetString(); }
std::string Comment::AuthorFlairText() { return CommentData()[Fields::AuthorFlairText].GetString(); }
std::string Comment::BodyMarkdown() { return CommentData()[Fields::BodyMarkdown].GetString(); }
long Comment::CreatedUtc() { return static_cast<long>(CommentData()[Fields::CreatedUtc].GetDouble()); }
bool Comment::IsSubmitter() { return CommentData()[Fields::IsSubmitter].GetBool(); }
std::string Comment::LinkName() { return CommentData()[Fields::LinkId].GetString(); }
std::string Comment::ParentName() { return CommentData()[Fields::ParentId].GetString(); }
bool Comment::IsStickied() { return CommentData()[Fields::Stickied].GetBool(); }

std::string Comment::SubmissionId() { return IdFromFullName(LinkName()); }
std::string Comment::ParentId() { return IdFromFullName(ParentName()); }

bool Comment::IsDeleted() { return CommentData()[Fields::Author].IsNull(); }
bool Comment::IsRemoved() { return !CommentData()[Fields::BannedBy].IsNull(); }
bool Comment::IsRoot() { return LinkName() == ParentName(); }

Submission& Comment::GetSubmission() {
    return _client.GetOrCreateSubmission(SubmissionId());
}

Comment* Comment::GetParent() {
    if (IsRoot()) {
        return nullptr;
    }

    auto& submission = GetSubmission();
    Comment* comment = submission.GetComment(ParentId());
    if (!comment) {
        Refresh();
        comment = submission.GetComment(ParentId());
    }
    return comment;
}

void Comment::Refresh() {
    const auto path = "comments/" + SubmissionId() + "/_/" + Id();
    auto data = _client.MakeRequest(path, http::verb::get, {{ "context", "100" }});

    // TODO: data[0] contains info on the submission; should we use that if the submission is not currently populated?

    for (auto& subTree : data[1]["data"]["children"].GetArray()) {
        // Should in theory only ever be one here, but the range-for loop covers us just in case
        Explore(nullptr, subTree["data"]);
    }
}

void Comment::Explore(Comment* parent, JsonValue_t& comment) {
    auto id = comment["id"].GetString();

    Comment* current;
    if (id == Id()) {
        current = this;
    } else {
        current = &GetSubmission().AddCommentIfNotExists(id, comment);
    }

    if (parent) {
        parent->_children.insert(id);
    }

    if (comment.HasMember("replies") && comment["replies"].IsObject()) {
        for (auto& subTree : comment["replies"]["data"]["children"].GetArray()) {
            Explore(current, subTree["data"]);
        }
    }
}

void Comment::FetchIfNecessary() {
    if (!_fetched) {
        auto fullDoc = _client.MakeRequest("api/info", http::verb::get, {{ "id", FullName() }});
        _json.CopyFrom(fullDoc["data"]["children"][0]["data"], _json.GetAllocator());
        _fetched = true;
    }
}

}
