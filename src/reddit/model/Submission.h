#pragma once

#include <memory>
#include <unordered_map>

#include "Comment.h"
#include "Replyable.h"

#include "utils/JsonUtils.h"

namespace PictureGame::Reddit::Model {

class Comment;

class Submission : public Replyable {
    public:
    Submission(RedditClient& client, std::string id);
    Submission(RedditClient& client, std::string id, const JsonValue_t& json);

    Submission(const Submission& other) = delete;
    Submission& operator=(const Submission& other) = delete;
    Submission(Submission&& other) = delete;
    Submission& operator=(Submission&& other) = delete;

    std::string FullName() const final { return _type + Id(); }

    std::string AuthorName();
    double CreatedUtc();
    bool IsSelf();
    std::string LinkFlairText();
    std::string Thumbnail();
    std::string Title();
    std::string Url();

    bool IsDeleted();
    bool IsRemoved();

    Comment* GetComment(const std::string& id) const;
    Comment& AddCommentIfNotExists(const std::string& id, JsonValue_t& doc);

    private:
    static constexpr auto _type = "t3_";

    bool _fetched{false}; // TODO: split this up to fetchedSubmission and fetchedComment
    JsonDoc_t _submissionJson;
    JsonDoc_t _commentJson; // ... Or, could we just get rid of this?

    std::unordered_map<std::string, std::unique_ptr<Comment>> _commentLookup;

    void FetchIfNecessary();

    const JsonDoc_t& SubmissionData() {
        FetchIfNecessary();
        return _submissionJson;
    }

    const JsonDoc_t& CommentListingData() {
        FetchIfNecessary();
        return _commentJson;
    }
};

}
