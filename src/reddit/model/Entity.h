#pragma once

#include <string>

namespace PictureGame::Reddit {
class RedditClient;
}

namespace PictureGame::Reddit::Model {

class Entity {
    public:
    Entity(RedditClient& client, std::string id): _client(client), _id(std::move(id)) {}

    virtual ~Entity() = default;

    const std::string& Id() const noexcept { return _id; }
    virtual std::string FullName() const = 0;

    protected:
    RedditClient& _client;

    private:
    std::string _id;
};

}
