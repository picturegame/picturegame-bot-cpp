#pragma once

#include "Entity.h"

namespace PictureGame::Reddit::Model {

class Replyable : public Entity {
    public:
    Replyable(RedditClient& client, std::string id);

    virtual ~Replyable() = default;

    void Reply(std::string&& message);
};

}
