#pragma once

#include <string>

namespace PictureGame::Reddit {

inline std::string IdFromFullName(const std::string& fullName) {
    return fullName.substr(fullName.find('_') + 1);
}

}
