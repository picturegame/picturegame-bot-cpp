#pragma once

#include <chrono>
#include <string>

#include "network/HttpClient.h"
#include "network/SslSession.h"

#include "utils/Logger.h"

namespace PictureGame::Reddit {

class AuthHandler {
    public:
    AuthHandler(
        std::string username,
        std::string password,
        std::string clientToken,
        std::string clientSecret,
        Utils::Logger& logger);

    AuthHandler(const AuthHandler& other) = delete;
    AuthHandler operator=(const AuthHandler& other) = delete;
    AuthHandler(AuthHandler&& other) = delete;
    AuthHandler operator=(AuthHandler&& other) = delete;

    const std::string& GetToken();

    private:
    using TimePoint_t = std::chrono::steady_clock::time_point;

    std::string _username;
    std::string _password;
    std::string _clientToken;
    std::string _clientSecret;

    std::string _accessToken;
    TimePoint_t _expiry;

    Network::HttpClient<Network::SslSession> _client;
    Utils::Logger& _logger;
};

}
