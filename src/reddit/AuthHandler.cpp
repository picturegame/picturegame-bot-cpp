#include "AuthHandler.h"

#include <rapidjson/document.h>

#include "network/HttpRequest.h"

namespace PictureGame::Reddit {

using namespace Network;
using namespace Utils;

constexpr auto SslUrl = "ssl.reddit.com";
constexpr auto AccessTokenPath = "api/v1/access_token";

namespace Fields {
    constexpr auto GrantType = "grant_type";
    constexpr auto Username = "username";
    constexpr auto Password = "password";
    constexpr auto RedirectUrl = "redirect_url";

    constexpr auto AccessToken = "access_token";
    constexpr auto ExpiresIn = "expires_in";
}

AuthHandler::AuthHandler(
    std::string username,
    std::string password,
    std::string clientToken,
    std::string clientSecret,
    Logger& logger):

    _username(std::move(username)),
    _password(std::move(password)),
    _clientToken(std::move(clientToken)),
    _clientSecret(std::move(clientSecret)),
    _client(SslUrl, logger),
    _logger(logger)
{}

const std::string& AuthHandler::GetToken() {
    const auto now = std::chrono::steady_clock::now();

    if (_expiry > now) {
        return _accessToken;
    }

    HttpRequest req(AccessTokenPath, http::verb::post);

    req.SetFormData({
        { Fields::GrantType, "password" },
        { Fields::Username, _username },
        { Fields::Password, _password },
        { Fields::RedirectUrl, "http://127.0.0.1" },
    });
    req.BasicAuth(_clientToken, _clientSecret);
    auto res = _client.SendRequest(std::move(req));

    rapidjson::Document doc;
    doc.Parse(res.body().c_str());

    _accessToken = doc[Fields::AccessToken].GetString();
    _expiry = now
        + std::chrono::seconds(doc[Fields::ExpiresIn].GetInt())
        - std::chrono::minutes(5); // Refresh the token early to avoid failing requests

    return _accessToken;
}

}
