#pragma once

#include <memory>
#include <vector>

#include "RedditClient.h"

#include "utils/FifoSet.h"

namespace PictureGame::Reddit {

template <typename Entity>
class ListingPoller {
    using Entity_t = std::unique_ptr<Entity>;
    using Iter_t = typename std::vector<Entity_t>::reverse_iterator;

    public:
    ListingPoller(RedditClient& client, std::string url): _client(client), _url(std::move(url)) {
    }

    ListingPoller(const ListingPoller& other) = delete;
    ListingPoller operator=(const ListingPoller& other) = delete;
    ListingPoller(ListingPoller&& other) = delete;
    ListingPoller operator=(ListingPoller&& other) = delete;

    bool Next() {
        return ++_iter != _entities.rend();
    }

    Entity& operator*() {
        auto& entity = *_iter;

        _seenIds.Add(entity->Id());
        _beforeName = entity->FullName();
        return *entity;
    }

    bool Refresh() {
        _entities.clear();

        Network::HttpRequest::FormData_t queryData {
            { "limit", std::to_string(PollLimit) }
        };
        if (!_beforeName.empty()) {
            queryData["before"] = _beforeName;
        }

        const auto response = _client.MakeRequest(_url, http::verb::get, std::move(queryData));

        for (const auto& json : response["data"]["children"].GetArray()) {
            const auto& data = json["data"];
            const auto id = data["id"].GetString();

            if (_seenIds.Contains(id)) {
                break;
            }

            _entities.push_back(std::make_unique<Entity>(_client, id, data));
        }

        _beforeName.clear();
        _iter = _entities.rbegin();
        return _iter != _entities.rend();
    }

    private:
    static constexpr size_t PollLimit = 50;

    RedditClient& _client;

    const std::string _url;
    std::string _beforeName;
    Utils::FifoSet<std::string> _seenIds{ PollLimit * 2 };

    std::vector<Entity_t> _entities;
    Iter_t _iter;
};

}
