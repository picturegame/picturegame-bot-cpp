#pragma once

#include <chrono>
#include <string>
#include <unordered_map>

#include <rapidjson/document.h>

#include "AuthHandler.h"
#include "model/Submission.h"

#include "network/HttpClient.h"
#include "network/SslSession.h"

#include "utils/Logger.h"

namespace PictureGame::Reddit {

class RedditClient {
    public:
    RedditClient(
        std::string username,
        std::string password,
        std::string clientToken,
        std::string clientSecret,
        Utils::Logger& logger);

    RedditClient(const RedditClient& other) = delete;
    RedditClient operator=(const RedditClient& other) = delete;
    RedditClient(RedditClient&& other) = delete;
    RedditClient operator=(RedditClient&& other) = delete;

    rapidjson::Document MakeRequest(
        const std::string& path,
        beast::http::verb verb,
        Network::HttpRequest::FormData_t&& query = {},
        Network::HttpRequest::FormData_t&& data = {});

    // TODO: Should we allow passing a json blob to instantiate it with?
    Model::Submission& GetOrCreateSubmission(const std::string& id);

    private:
    using TimePoint_t = std::chrono::steady_clock::time_point;

    Network::HttpClient<Network::SslSession> _client;
    AuthHandler _auth;
    Utils::Logger& _logger;

    double _rateLimitRemaining;
    TimePoint_t _rateLimitResetTime;

    std::unordered_map<std::string, std::unique_ptr<Model::Submission>> _submissionLookup;
};

}
