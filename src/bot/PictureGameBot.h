#pragma once

#include "Config.h"

#include "api/ApiClient.h"
#include "api/model/Round.h"

#include "reddit/ListingPoller.h"
#include "reddit/RedditClient.h"

#include "reddit/model/Comment.h"
#include "reddit/model/Submission.h"

#include "utils/Logger.h"

namespace PictureGame::Bot {

class PictureGameBot {
    public:
    explicit PictureGameBot(const Config& config);

    PictureGameBot(const PictureGameBot& other) = delete;
    PictureGameBot& operator=(const PictureGameBot& other) = delete;
    PictureGameBot(PictureGameBot&& other) = delete;
    PictureGameBot& operator=(PictureGameBot&& other) = delete;

    void Run();

    Utils::Logger& GetLogger() {
        return _logger;
    }

    private:
    Utils::Logger _logger;

    Api::ApiClient _api;
    Reddit::RedditClient _reddit;

    Reddit::ListingPoller<Reddit::Model::Comment> _comments;
    Reddit::ListingPoller<Reddit::Model::Submission> _submissions;

    Api::Model::Round _currentRound;
};

}
