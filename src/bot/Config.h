#pragma once

#include <string>

#include "utils/Logger.h"

namespace PictureGame::Bot {

struct Config {
    const char* RedditUsername;
    const char* RedditPassword;
    const char* RedditClientToken;
    const char* RedditClientSecret;

    const Utils::LogLevel LogLevel;

    const char* ApiHost;
    const char* ApiPort;
    const char* ApiUsername;
    const char* ApiPassword;

    const std::string SubredditName;
};

}
