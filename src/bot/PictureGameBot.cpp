#include "PictureGameBot.h"

namespace PictureGame::Bot {

PictureGameBot::PictureGameBot(const Config& config):
    _logger(config.LogLevel),
    _api(config.ApiHost, config.ApiPort, config.ApiUsername, config.ApiPassword, _logger),
    _reddit(config.RedditUsername, config.RedditPassword, config.RedditClientToken, config.RedditClientSecret, _logger),
    _comments(_reddit, "r/" + config.SubredditName + "/comments"),
    _submissions(_reddit, "r/" + config.SubredditName + "/new"),
    _currentRound(_api.GetCurrent())
{
    _logger.Info("Bot initialized", {
        { "roundNumber", std::to_string(_currentRound.RoundNumber()) },
        { "url", "https://redd.it/" + _currentRound.Id() },
        { "ongoing", std::to_string(_currentRound.IsSolved()) },
    });
}

void PictureGameBot::Run() {
    const bool newComments = _comments.Refresh();
    if (newComments) {
        for (; _comments.Next();) {
            auto& comment = *_comments;
            _logger.Info(std::to_string(comment.CreatedUtc()) + ": " + comment.BodyMarkdown());
        }
    }
}

}
