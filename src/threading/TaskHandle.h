#pragma once

#include <tuple>

#include "Semaphore.h"

namespace PictureGame::Threading {

template <typename ...Args>
class TaskHandle {
    public:
    using Result_t = std::tuple<Args...>;

    TaskHandle() = default;

    TaskHandle(const TaskHandle& other) = delete;
    TaskHandle& operator=(const TaskHandle& other) = delete;
    TaskHandle(TaskHandle&& other) = delete;
    TaskHandle& operator=(TaskHandle&& other) = delete;

    void Complete(Args... result) {
        _result = std::make_tuple<Args...>(std::forward<Args>(result)...);
        _sem.Notify();
    }

    Result_t GetResult() {
        _sem.Wait();
        return std::move(_result);
    }

    private:
    Result_t _result;
    Semaphore _sem;
};

}
