#pragma once

#include <condition_variable>
#include <mutex>

namespace PictureGame::Threading {

class Semaphore {
    public:
    explicit Semaphore(int count = 0): _count(count) {
    }

    void Notify(size_t count = 1) {
        std::scoped_lock lock(_mtx);

        while (count-- > 0) {
            ++_count;
            _cv.notify_one();
        }
    }

    void Wait() {
        std::unique_lock lock(_mtx);
        while (_count <= 0) {
            _cv.wait(lock);
        }
        --_count;
    }

    private:
    int _count;
    std::mutex _mtx;
    std::condition_variable _cv;
};

}
