#include "ApiClient.h"

#include "network/HttpRequest.h"

namespace PictureGame::Api {

using namespace Model;
using namespace Network;
using namespace Utils;

ApiClient::ApiClient(
    const char* host,
    const char* port,
    std::string username,
    std::string password,
    Logger& logger):

    _username(std::move(username)),
    _password(std::move(password)),
    _client(host, port, logger),
    _logger(logger)
{}

Round ApiClient::GetCurrent() {
    const auto response = MakeRequest("/current", http::verb::get);

    rapidjson::Document roundDoc;
    roundDoc.CopyFrom(response["round"], roundDoc.GetAllocator());
    return Round(std::move(roundDoc));
}

void ApiClient::PostRound(const Round& round) {
    MakeRequest("/rounds", http::verb::post, round.Json());

    // TODO: Check status code (likely need to change MakeRequest to return more than just the json body?)
}

rapidjson::Document ApiClient::PatchRound(const Round& round) {
    const auto path = "/rounds/" + std::to_string(round.RoundNumber());
    auto response = MakeRequest(path, http::verb::patch, round.Json());

    // TODO: Check status code

    return response;
}

void ApiClient::DeleteRound(int roundNumber) {
    MakeRequest("/rounds/" + std::to_string(roundNumber), http::verb::delete_);
}

rapidjson::Document ApiClient::MakeRequest(
    const std::string& path,
    http::verb verb,
    const rapidjson::Document& data)
{
    HttpRequest request(path, verb);
    if (data.IsObject()) {
        request.SetJsonData(data);
    }
    request.BasicAuth(_username, _password);

    auto res = _client.SendRequest(std::move(request));

    rapidjson::Document doc;
    doc.Parse(res.body().c_str());
    return doc;
}

}
