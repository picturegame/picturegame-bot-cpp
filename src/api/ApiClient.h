#pragma once

#include <rapidjson/document.h>

#include "api/model/Round.h"

#include "network/HttpClient.h"
#include "network/HttpRequest.h"
#include "network/PlainSession.h"

#include "utils/Logger.h"

namespace PictureGame::Api {

class ApiClient {
    public:
    ApiClient(
        const char* host,
        const char* port,
        std::string username,
        std::string password,
        Utils::Logger& logger);

    ApiClient(const ApiClient& other) = delete;
    ApiClient& operator=(const ApiClient& other) = delete;
    ApiClient(ApiClient&& other) = delete;
    ApiClient& operator=(ApiClient&& other) = delete;

    Model::Round GetCurrent();

    void PostRound(const Model::Round& round);
    rapidjson::Document PatchRound(const Model::Round& round);
    void DeleteRound(int roundNumber);

    private:
    const std::string _username;
    const std::string _password;

    Network::HttpClient<Network::PlainSession> _client;
    Utils::Logger& _logger;

    rapidjson::Document MakeRequest(
        const std::string& path,
        http::verb verb,
        const rapidjson::Document& data = {});
};

}
