#pragma once

#include <string>

#include <rapidjson/document.h>

namespace PictureGame::Api::Model {

class Round {
    public:

    Round();
    explicit Round(rapidjson::Document&& json);

    Round(const Round& other) = delete;
    Round& operator=(const Round& other) = delete;
    Round(Round&& other) = default;
    Round& operator=(Round&& other) = default;

    int RoundNumber() const;
    std::string Id() const;

    bool IsSolved() const;

    void SetRoundNumber(int roundNumber);

    void SetTitle(const std::string& title);
    void SetPostUrl(const std::string& url);
    void SetThumbnailUrl(const std::string& url);
    void SetId(const std::string& id);
    void SetWinningCommentId(const std::string& id);

    void SetHostName(const std::string& name);
    void SetPostTime(long timestamp);

    void SetWinnerName(const std::string& name);
    void SetWinTime(long timestamp);
    void SetPlusCorrectTime(long timestamp);

    const rapidjson::Document& Json() const {
        return _json;
    };

    private:
    rapidjson::Document _json;

    template <typename T>
    void SetValue(const char* field, T value) {
        _json.AddMember(rapidjson::StringRef(field), value, _json.GetAllocator());
    }
};

}
