#include "Round.h"

namespace PictureGame::Api::Model {

namespace Fields {
    constexpr auto RoundNumber = "roundNumber";

    constexpr auto Title = "title";
    constexpr auto PostUrl = "postUrl";
    constexpr auto ThumbnailUrl = "thumbnailUrl";
    constexpr auto Id = "id";
    constexpr auto WinningCommentId = "winningCommentId";

    constexpr auto HostName = "hostName";
    constexpr auto PostTime = "postTime";

    constexpr auto WinnerName = "winnerName";
    constexpr auto WinTime = "winTime";
    constexpr auto PlusCorrectTime = "plusCorrectTime";
}

Round::Round() {
    _json.SetObject();
}

Round::Round(rapidjson::Document&& json): _json(std::move(json)) {
}

int Round::RoundNumber() const { return _json[Fields::RoundNumber].GetInt(); }
std::string Round::Id() const { return _json[Fields::Id].GetString(); }

bool Round::IsSolved() const { return _json.HasMember(Fields::WinnerName) && _json[Fields::RoundNumber].IsString(); }

void Round::SetRoundNumber(int roundNumber) { SetValue(Fields::RoundNumber, roundNumber); }

void Round::SetTitle(const std::string& title) { SetValue(Fields::Title, rapidjson::StringRef(title.c_str())); }
void Round::SetPostUrl(const std::string& url) { SetValue(Fields::PostUrl, rapidjson::StringRef(url.c_str())); }
void Round::SetThumbnailUrl(const std::string& url) { SetValue(Fields::ThumbnailUrl, rapidjson::StringRef(url.c_str())); }
void Round::SetId(const std::string& id) { SetValue(Fields::Id, rapidjson::StringRef(id.c_str())); }
void Round::SetWinningCommentId(const std::string& id) { SetValue(Fields::WinningCommentId, rapidjson::StringRef(id.c_str())); }

void Round::SetHostName(const std::string& name) { SetValue(Fields::HostName, rapidjson::StringRef(name.c_str())); }
void Round::SetPostTime(long timestamp) { SetValue(Fields::PostTime, timestamp); }

void Round::SetWinnerName(const std::string& name) { SetValue(Fields::WinnerName, rapidjson::StringRef(name.c_str())); }
void Round::SetWinTime(long timestamp) { SetValue(Fields::WinTime, timestamp); }
void Round::SetPlusCorrectTime(long timestamp) { SetValue(Fields::PlusCorrectTime, timestamp); }

}
