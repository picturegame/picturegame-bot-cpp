#!/bin/sh

REBUILD=0
MODE=Release

BIN_FOLDER_BASE=bin
BIN_FOLDER=$BIN_FOLDER_BASE/$MODE

ParseCmdLine() {
    while getopts "rm:" OPT; do
        case "${OPT}" in
            r)
                echo "rebuilding selected"
                REBUILD=1
                ;;
            m)
                MODE=${OPTARG}
                BIN_FOLDER=$BIN_FOLDER_BASE/$MODE
        esac
    done
}

Rebuild() {
    rm -rf $BIN_FOLDER
    cmake . -B$BIN_FOLDER -DCMAKE_BUILD_TYPE=$MODE
}

ParseCmdLine $@

if [ ! -d "${BIN_FOLDER}" ]; then
    REBUILD=1
fi

if [ ${REBUILD} -gt 0 ]; then
    Rebuild
fi

echo "building with mode $MODE..."
cmake --build $BIN_FOLDER -- -j3
