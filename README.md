# picturegame-bot-cpp

A rewrite of [picturegame-bot](https://gitlab.com/picturegame/picturegame-bot), from the ground up, in modern C++.

This project is mainly an experiment/learning exercise in C++, but may eventually turn into a fully-featured bot
which can replace the existing Python implementation.

The project is developed on a Linux system, and is not tested on any other system.
Additional setup is likely required to get it to build in Windows; I have no interest in making this work.

## Dependencies

* `boost` v1.71.0 (headers only)
* `rapidjson` v1.1.0
* `cmake` (tested with v3.15.4)
* `gcc` v9 (no other compilers have been tested)

## Configuration

Config is currently managed by a header file not checked into the repo.
Setting up proper loading of config files is planned.

## Building

* Run `./build.sh -m <Debug|Release|RelWithDebInfo>`
* To perform a full rebuild of a given configuration, run with the `-r` flag.

## Running

* Run `./bin/<Debug|Release|RelWithDebInfo>/picturegame-bot`
